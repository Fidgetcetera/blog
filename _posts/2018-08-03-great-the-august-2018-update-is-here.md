---
layout: post
title: "Great! The August 2018 Update is Here!"
date: 2018-08-03
tags: [General, Updates]
content-warnings: "College, parents, Patreon and money situation"
---
It's August! Let's get into what we've done last month and what we will be doing this month!

<!--more-->

So, first off: No zines or jobs for Acey currently, but our work should show up in INDUSTRY STANDARD eventually! It's just not out yet. The portrait zine, on the other hand, didn't work out yet. Also, LGBTQ+ wrath month is over (we suggested gluttony, but everyone was a bit more on top of wrath last month), and that was fun.

Other fun things include our new [Twine Projects page](https://acey.eerie.garden/twine/) that exists on our site!! Right now, there's only one thing on there, a pronoun.is inspired pronoun-selector template for anyone to use with credit!! We are really really looking forward to making and releasing more Twine stuff. We'll probably be putting everything we make for free on that page, but also making an itch.io page where people can pay what they wish for them.

We'll be working to put out a [Transposable](https://stipes.co/series/transposable) episode or two this month, since we haven't done one since the end of June.  Hopefully we can get two in. At least one will go into accessibility and listening to disability activists! We might not manage to get out more than one, however.

The reason we'd not get anything else done, though, is because we'll be back in college in a few weeks! So we'll be readjusting to everything, moving into an apartment, figuring out our food budgeting and all that. Lots of work, weee.

Speaking of food budgeting, this does bring us to another important point, regarding money. Just to be open and honest, we got financial aid and student loans to pay for tuition and a portion of housing, and the rest of the money was taken care of, as our parents took out a credit card and then payed it with that, and are now going to be paying that off over time. So, we're in a better spot than we could be, and we want to be super clear that yes, we have support from our parents and it's a very real thing.

With that said, we haven't worked out with our parents anything regarding paying for food and basic necessities at the apartment we'll be living at at school, and don't really know what the situation will be there. We don't have a job, because it is still hard to find work as a queer college cripple that's permanently exhausted. We'll keep trying to make weird shit online here so y'all have some reason to give us money to exist.

So basically, we're gonna say again, if you wanna give us money on [Patreon](https://patreon.com/fidgetcetera) to make sure we have money, food, meds, and possibly just, the ability to have extra spending money for small things or medical issues or whatever, please feel free. It's okay if you don't, but anything you can is appreciated. We also have [Paypal](https://paypal.me/fidgetcetera) and [Ko-fi](https://ko-fi.com/fidgetcetera) for this. Also, if you know us, don't hesitate to ask us for our personal Paypal instead, so less fees get taken out of what you send. It's nice not to doxx ourselves to the entire internet when receiving money, though.

Also, if you do give anyone on Patreon money via debit or credit card, you may want to check out your card declines - Patreon probably emailed you about this already, but they [kinda fucked up](https://twitter.com/Patreon/status/1025071287020871680) and had a bunch of cards decline recently. Our own patrons seem to have it all resolved now, which is good.
