---
layout: post
title: "Ah Fuck, It's July 2018 (Time for an update!)"
date: 2018-07-01
tags: [General, Updates]
content-warnings: "Food jokes, Patreon and money situation"
---

Well! June is over, july is here! Yeah, technically, we started doing the updates midway through the month, but we wanted to get back to them being at the start.

<!--more-->First off! LGBTQIA+ pride month? Over! You know what that means? LGBTQIA+ gluttony month!! Give your friends (that want it) food! Technically this isn't some sort of real consolidated thing, but you can participate anyway. Just saying, we've got [ko-fi](https://ko-fi.com/fidgetcetera) and [paypal](https://www.paypal.me/Fidgetcetera) and [patreon](https://www.patreon.com/Fidgetcetera) on the [homepage](https://acey.eerie.garden) if you wanna pay for us getting a pizza or something~

Also, pride every month!! Keep waving your flags and being wonderful!! (Yes, you too, asexual and aromantic spectrum folx!)

So during June, we did already talk about figuring out being plural. That's still very much a thing. We hosted [a Transposable episode](https://stipes.co/podcast/transposable-episode-20-a-box-of-gay/) for the first time since that point! (And also just the first time in a while.) Otherwise we've just been slowly figuring stuff out! ^w^

We've also submitted art for a zine, which we'll absolutely post about if our art makes it in! And we've sent our résumé in for a potential amount of work, which feels fuckin weird. We're also going to be submitting photos for a [nonbinary portrait zine](https://www.instagram.com/p/Bj7x9zDjVjs/?taken-by=riotcakes), which closes the 10th! Zines seem really cool and quite honestly we wanna make art for them, and really just have specific projects to make art for. The only worry is that really we want specific goals to make art for and without major time limits and pressure. Trying to find what works well for us with art is hard as hell, but we'll try!

Again, we'd love to put our art (and photography) in one consolidated place online, but we're still figuring out where's best. Hopefully, just somewhere on this same domain!!

And finally, just a wrapup of our patreon stuff. We've currently got enough to pay for monthly costs (Though, Monday we find out if that will remain. Insurance is a pain) as well as $17 a month extra!! We could do something nice for ourselves, celebrate gluttony month! Yay~! Thanks again to everyone for supporting us!! 💚💙
