---
layout: post
title: "End of October 2018, Update Time!"
date: 2018-10-27
tags: [General, Updates]
content-warnings: "College, money situation (positive)"
---
It's October! This update's extra hella late, oops.

<!--more-->
First off, sorry for the lateness! We've had other writing we were prioritizing, with a lot of papers due for the semester. We've been a bit stressed out with school stuff, and have gone through exams and midterms. Not as badly as we've been stressed prior, but enough to have us push social media and blog updates to the sidelines a bit.

We've started livestreaming a little on [Mixer](https://mixer.com/Fidgetcetera) which has been good! We'll be looking into reviving our [Twitch channel](https://twitch.tv/Fidgetcetera) sometime, as well.

Unfortunately, really, we haven't gotten a lot done otherwise. Transposable has been on hold due to chosa's life stuff (but it might be back very soon!!), and we haven't really had the focus to get a lot of stuff (like, blog posts for example) done.

Money-wise we're doing pretty okay though! One of our medications went down in price when we switched pharmacies again (that pharmacy switch happens depending on whether we're with our parents or living in Atlanta), so college move-in has led to good stuff on that front, meaning our [Patreon goals](https://patreon.com/Fidgetcetera) have been updated! We're making enough to pay for monthly costs and a bit extra right now. We really appreciate that!!!
