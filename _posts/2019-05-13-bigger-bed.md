---
layout: post
title: "We're Going to Need a Bigger Bed: a polyamorous shipping game"
date: 2019-05-13
tags: [roleplaying game]
content-warnings: "silly polyamory stuff!"
---
We wrote this game on social media, but it seemed right to share now that it's named! Please enjoy We're Going to Need a Bigger Bed!

<!--more-->
# Instructions:
- Everyone at the table makes up one or chooses one existing character, in private. You can roll dice to pick existing fictional characters, you can make self insert OCs, you can literally make yourself.

- You all, as a group, decide how these characters fit together in relationships, roleplaying scenes between them as you like!

- Graph the resulting web of connections.

- Repeat this process, but now you're introducing new characters to the existing polycule.

- Continue play until you have had your fill of fun.

- And remember, no cops at pride!

The game was named by [bonnie](http://bonnie.eerie.garden), who you should give your money! Additionally, consider [polycul.es](https://polycul.es/) as a fun way to do the graphing!
