---
layout: post
title: Month's End, August 2017
date: 2017-08-30
tags: [General, Updates, Patreon Export]
content-warnings: "College"
---
Hey everyone! It's almost the end of August, so I figured I should get y'all caught up on where I'm at with my life!

<!--more-->  Sooooo:

I moved into my college dorm room and have my class schedule decently figured out! I'm taking elementary Arabic lessons alongside my Chemistry, Algebra, World History, and Human Communications. I'm settling in, making meatspace friends, and learning!

I've established a sort of thing for my music, and maybe other 18+ endeavours, found [here (18+!!!)](https://instance.business/@glittercumslimegirl)! When I actually make music I'll probably set up a Twitter account as well.

[Transposable](https://stipes.co/series/transposable) has been going along well! Cristian and I talking about queer stuff is really fun, and you can keep your eyes open for weekly episodes of this stuff. I also might or might not have at least 2 more podcast ideas swirling around in my brain.

Thanks for supporting me, everyone! ^-^
