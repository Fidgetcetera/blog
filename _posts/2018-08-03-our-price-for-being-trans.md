---
layout: post
title: "Our Price for Being Trans"
date: 2018-08-03
tags: [Trans stuff]
content-warnings: "Medical situations, money, hormone replacement therapy, transmisia, mention of sexual assault"
---
Hey, you know what's real fucked up? How much it's costed us for our medical transition.

<!--more-->$4209.60. Four thousand, two hundred and nine dollars, and sixty cents.

That's a rough estimate of our medical costs since starting transitioning in 2016. Our expense tracking isn't exact on what doctors appointments were about, some $35 doctors visits that weren't about HRT might have slipped in, others that were about it might have slipped out, we might've bought some stuff at rite-aid and checked it out at the same counter as our meds. But, it's a decent estimate. This is solely us on HRT, no surgeries, no hair removal, nothing but hormones.

So how does medical transition get to cost so much? Well, let's break down some of the biggest costs. We hunted for an endocrinologist who would allow us to get on HRT, after having been in therapy for a while. The first one we tried charged $200 up front, wouldn't take insurance, sexually assaulted, and misgendered us, at age 16-17 (it's hard to remember), when we were a trans girl. We don't remember the name of that clinic or we would name-and-shame into the fucking ground, and this was one of the small gaps in our expense tracking. But we do remember that price.

Needless to say, we didn't go back to the last place. _Months_ later we found the Jacksonville Center for Reproductive Medicine. Pretty far away, so travel costs were an issue. Also Had to have another therapy appointment just to talk about getting a letter saying we were very definitely experiencing gender dysphoria, just in case we were a confused cis person! How horrible that would be (sarcasm). Their appointments each costed around $100. They required a multitude of blood tests, and insurance would cover them, so it'd be $20-$40.
Well, insurance used to cover them. Our parents, who we get insurance through currently (thankfully we have that much support), had their work end up switching providers. Now, UnitedHealthcare receieved funding from the Affordable Care Act. And a portion of that meant [UnitedHealthcare couldn't exclude trans people's care](https://broker.uhc.com/articleView-17051). However, fun story: UHC still had it in their plan that trans coverage wouldn't be included.

So essentially, new insurance copied the UHC plan word-for-word, wasn't being funded by the ACA, and thus didn't have to comply to the rules UHC did. There's a lawsuit someone can fight there, if someone wasn't dealing with having very little money due to this change. Fun, right?

So new insurance, new rules, despite "nothing will change" assurances. Norethindrone, an artificial progestin we were put on, was not covered. Because it was priorly covered, we were told we could file it and "grandfather" it in, but then filing it got literally no response. New $90 a month cost.

Bonus round, the Jacksonville Center for Reproductive Medicine put that our blood test were for transgender health directly on blood test forms. Insurance loved that, real money-saver! So then that was another $570-ish. And guess what, there's still another blood test bill that's been looming over us since August of last year. Literally almost a year old. Waiting for insurance to decline so they can charge us more!

This wasn't all the costs, again, just a rough overview of the more expensive stuff. These won't all add up if you do the math, we know. We didn't provide all the information for that to even remotely be possible But hopefully it gives you a good idea as to why it costs so fuckin' much, even without any surgery.

So yeah, being trans is fucking expensive, for us as well as others. If you'd like to help us out with that, our [Patreon](https://www.patreon.com/Fidgetcetera), [Paypal](https://www.paypal.me/fidgetcetera), and [Ko-fi](https://ko-fi.com/fidgetcetera) are available. If you know us, feel free to reach out before paying on Paypal. They take a fee automatically for business accounts depending on how you pay and it gets fucky, but we have a personal account as well (however, not doxxing ourselves and getting paid as Fidgetcetera is useful).
