---
layout: post
title: Spirituality and/or Religion?
date: 2017-04-08
tags: [Personal]
content-warnings: "Witchcraft, religion (USA-focused experiences)"
---
I've been meaning to write this for a little while, but input from people on the whole thing where I have a Discord server called the "Fidgetcetera Coven" and how I call myself a witch has kinda made me wanna put up a little more talk. So I guess I'll start out with the plain and simple - I'm not wiccan, neopagan, or really able to find myself in any kind of religious or spiritual thing at the moment.

<!--more--> I took on the label of "witch" because well, witchcraft is traditionally just anything a woman did that scared men enough to burn her at the stake for it. I also like the silly felt tip hat, as you can guess from the selfies I've posted.

But anyway, back to religion. So, my family's a little odd. My dad was raised in a rather Christian home (thankfully, almost all of his side of the family are really amazing and nice about my being trans), but has kinda been agnostic/atheist, with a past hankering for making fun of conservative Christians on the internet. I think he's stopped doing that last bit so much, but I don't really know. My mom was wiccan at some point but didn't really practice anything heavily and never really introduced me to any of it. So I got raised mostly without religion. It was kinda cool? I mean, I'm at least anecdotal evidence that the whole "you can't have morals without religion" thing ain't true. Either that, or believing that the resources we have throughout humanity should be at the very least distributed enough that no one has to starve is not actually related to morals, somehow.

I grew up with occasional visits to the extended family - My grandmother on mom's side was always a little extreme. Video games are corrupting youth, D&D is Satanism, etc. On dad's side, everyone was relatively chill, with a healthy dose of slight limitation on the time spent on the TV before we had to go outside. So I kinda had two sides of the common American religious views and kinda decided none of that was for me. I had a bit of a phase of "hah, God isn't real, magic sky fairy blah blah blah," but I've since stopped mocking religion as a general thing because it feels a bit like a dick move.

So at this point, I just don't care. I guess that makes me agnostic, or something? I just want people to stop using religion or lackthereof to excuse hateful and ignorant bullshit, I guess.
