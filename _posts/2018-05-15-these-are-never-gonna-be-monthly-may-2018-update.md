---
layout: post
title: "These are Never Gonna Be Monthly&#58; A May 2018 Update"
date: 2018-05-15
tags: [General, Updates]
content-warnings: "Governmental name change, money, my living situation, chronic pain, college mention, parents"
---

Hey, at least it's almost exactly 2 months off from the last one! Consistency is something!!  

<!--more-->So okay, what's been going on in Acey's life, huh? Well. First off, I am now eliminating my deadname from my life. Woo! Hard shit to deal with, honestly. Changing your government alias is a whole pain in the ass even just with having to deal with individual places that have documents on the government side of things, and then you factor in non-government entities such as colleges or paypal which can mysteriously lose your forms and not verify them for a month... It's a whole fucking process.

Other big things: I'm done with school! More time for personal projects, maybe! Like, actually maintaining this blog and my personal site and updating it in ways I want to, even. Who knows, maybe I'll write some porn and throw that somewhere, too. Or make some custom emoji... 🤔

I've also been embracing that I'm disabled way more, posting about it on my [Mastodon](https://sins.center/@fidgetcetera) and using my cane (it's named Neptune now) and stuff. It's been really nice to not push myself too much! I had a major injury when I was a kid, I won't get into it but basically my muscles n nerves on my left leg are all hecked up. I'm doing a lot better since starting using a cane and everything. Way less days where I can't get out of bed in the morning because of my pain.

My [patreon](https://www.patreon.com/fidgetcetera) has been something I've been a little more in need of lately, on the maybe-less-positive. I'm living with my parents, and they really want me to find a job or have income. The aforementioned leg injury means that standing for hours on end more than maybe 1 or 2 times a week is hard - This kinda puts me in a bad spot for finding most jobs in a tiny town in rural Georgia. So while I'll be trying for what few possibilities I have, I really would appreciate having alternatives as far as income goes.

Also, I'm gonna say thanks to all my current donors! I aaaalmost get enough to pay for like, all my meds without issue!! That's super huge and great. I have some mental health struggles to work through about actually asking for the help I need, but I've been doing my best to sort that out and stuff. Thank you all!!

I think that's the most of it! Thanks for reading and keeping up. I'll try to do a blog post that isn't one of these monthly updates again Soon™!
