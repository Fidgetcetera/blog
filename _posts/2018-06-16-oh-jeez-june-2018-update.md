---
layout: post
title: "Oh jeez! The June 2018 Update"
date: 2018-06-17
tags: [General, Updates]
content-warnings: "Patreon and money situation, self-discovery, web dev shit"
---

Hey! We're here, we're queer, we've figured some shit out! Let's get to the month's happenings!

<!--more-->So, first of all, we're using first person plural pronouns, 'cause, we're plural. Collectively, we're Ace Jaycee, but apart inside our head is Nemmy Nyms (Hi, I'm the one who's usually rolling out these blog posts because I like to type like this), and Salix Quercus (hi i'm fuckin queer!!!!). This is some stuff we've wondered on for a bit, and it feels weird, as Nemmy didn't exactly come to us recently. We've been these two voices in our head as long as we can remember, but never quite had a face for Nemmy or separated hir from Salix (previously just also Ace Jaycee before ze figured out something better identity-wise). If you're interested in reading about this at all there's some resources on [Are We Plural](http://areweplural.com/) which could be helpful.

Anyway, with that out of the way! Fun stuff. So the blog is updated. The RSS feed probably got all jumbled, sorry! We retroactively added content warnings. We're using the [:target CSS selector](https://developer.mozilla.org/en-US/docs/Web/CSS/:target) to show/hide the post depending on the ID of the page. That's a technical way of saying "We have a nice button that hides and shows the content warned content appropriately now." If you ever share posts, pleeease make sure to copy the link with the content warning closed, and don't remove the # part of the url!

On to other exciting things! Our [patreon](https://patreon.com/fidgetcetera) is in a cool situation. If you remember last post, we mentioned that we're currently living with parents and getting work is a pain right now. We've gotten things to a point where the money we get from people can pay for all our meds! This is really something. We're not in a perfect position, because we uh, kind of still have no clue how we'll pay for the next semester of college, but. We're really glad to get something at all and to be able to afford meds. Thank you all so much!

Also, apologies for lack of updates on some stuff. Transposable hasn't had an episode for a bit, we'll have to try to get one going. We did, however, guest on a podcast, [What's Yr Fursona?](https://soundcloud.com/wyfcast/episode-26-ace) which was super fun!!! [This twitter thread](https://twitter.com/WYFcast/status/1002680598815576064) also contains images we talked about on the show! We also obviously have barely kept this blog alive (Though hey wow this one came out this month go us). We have made some art though!!! We're excited about that.

We made [this robot hand](https://sins.center/@Fidgetcetera/100165501252004572) as Friends At the Table: Counter/Weight fanart, [these glitched images](https://sins.center/@Fidgetcetera/100129946052671844) as some self expression, and honestly we've made a bunch more. We'd like to have a portfolio page for all this art somewhere, but we're still figuring that out. Going to [our mastodon media page](https://sins.center/@Fidgetcetera/media) is a good way to get a view of some of our art for now (albeit with some selfies and other stuff tossed in).
