---
layout: post
title: Halloween Resurrection
date: 2017-10-31
tags: [General, Updates, Personal, Queerness]
content-warnings: "Money"
---
It's been a while, huh? Just under 7 months without posting to the blog! This'll just be some housekeeping stuff.

<!--more-->So first up, stuff I've done! I started school!! I also have a [Patreon](https://patreon.com/Fidgetcetera) now! You can shove money at me, if ya want. I'm gonna use that to draft things, host polls, and maybe other stuff? Idunno, I'll figure it out. I host a podcast with chosafine on [Stipes Radio](https://stipes.co/)! The rest you can read on the bio page, tbh.

But hey also! I'm nonbinary now! I'm AJ. You can use [they/them pronouns](https://pronoun.is/they/.../themself) for me. I'm also aromantic, bisexual, and still **QUEER AS FUCK**. I'llll probably do another big blog post like [i'm gay]({{"2017/04/04/im-gay.html" | relative_url}}).
