---
layout: post
title: Oops!! A late update! Sept/Oct 2017
date: 2017-10-18
tags: [General, Updates, Patreon Export]
content-warnings: "College"
---

Hi everyone!!! So I realize it is mid-October. I am so sorry to everyone subscribed!

<!--more-->So first up, over this chunk of time - I'm nonbinary and aromantic! I've been sticking to being called AJ now. Alice isn't not my name, but AJ is nice! And please use they/them pronouns, rather than she/her. I'll update my page itself on that soon!

And second of all, school! That's the reason I haven't been keeping up, but I think it's nice to update y'all on the fact that I'm keeping my grades up decently! My teachers aren't being consistent about uploading my grades but I should have above a 90% in every class except maybe Chem (My professor hasn't decided on how the grades will be weighted for quizzes vs homework).

Now on to real things you can consume!! Gosh. Transposable has been keeping up okay but we've had a few hiccups - chosa needed a break, we recorded another episode, and then their computer broke! Soooo that's a little on hold. Otherwise, I've tried to participate in inktober but that's not come along a lot, I'll be sure to post what I've done though!

Thank you to everyone supporting me, and I'll try to keep stuff going in spite of the tough load of school work!
