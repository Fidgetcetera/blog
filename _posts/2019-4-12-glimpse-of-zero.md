---
layout: post
title: "a glimpse of zero"
date: 2019-04-12
tags: [poetry, disability]
content-warnings: "pain and medical stuff"
---
we decided to write some poetry while we were in pain in the shower this morning:


<!--more-->

>"On a scale from one to ten,

>how much pain are you in?"



what is ten?


>"One is next to nothing,

>and ten is the worst you have experienced."

---

how can there be anything but one?

in what universe does anything even fairly reach one fifth of the most pain we have felt?

---

what is a scale from one to ten when you are constantly in pain?

when your baseline of a good day is when you went to the kitchen and it felt like you did 20 squats to get there?

what is zero? what does a lack of pain feel like?

we have forgotten that feeling.

---

we catch glimpses of zero.

our brain spares us.

dissociates from all bodily sensation.

we lose time, we lose focus, but, for a moment,

at least we lose some feeling of pain.

---

it's a ghost now.

a pain like hearing muffled music

a glimpse of zero
