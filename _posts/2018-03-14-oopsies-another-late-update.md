---
layout: post
title: Oopsies! Another late update.
date: 2018-03-14
tags: [General, Updates]
content-warnings: "Money, college"
---

Hey remember that time I was gonna do this monthly and keep updating you all on my life? Yeah uh, oops. Here's how the past couple of months have gone so far!  

<!--more-->First up, I uh, registered myself for 16 credit hours of classes (5 classes in total) and realized that was actually bad and dropped a class. Now I'm readjusting to having real amounts of free time (mostly, with a lot of playing Terraria). School has been going along well enough, I've had to shift gears and really put some heavy focus in my chemistry class but I'm feeling good and confident about the whole thing.

In this time, chosa and I have put out like, uh, two [Transposable](https://stipes.co/series/transposable/) episodes. I'm enjoying the lack of scheduling with that show, though I want to make more things! I've been trying to half-assedly plan things with people to maybe make more fun things. No guarantees that I actually put out things, considering I've been posting about wanting to make new podcasts for like, 5 months now? But y'know. We'll see. Mostly what I've accomplished online is posting a lot of things on Mastodon and Twitter, though. (I don't consider this bad). Uh, also, my RSS feed link is broken. I'm aware of that and not really sure how to fix it right this second. Sorry!

The big real life news: I've filed my legal name change!!! By April 4th my government name shall be corrected. It took a lot of effort to not up the number of middle names I have and put in jokey things so I was "Ace Fucking Jaycee" or something. This is super exciting for me! ALSO!!!! Y'all amazing patreon donors have kept me safe and actually able to do that, and I now get $34 a month, which is enough to pay for my monthly medications! Thank you! ^wwwwwww^
