---
layout: post
title: "Nemnem's Pizza Dough"
date: 2019-05-05
tags: [Recipe]
content-warnings: "food!"
author: nemmy
---

🐯Hey, Nemmy Nyms here! I made a pizza dough recipe based on a lot of things I use and my own "give or take a lot" methods of timing and measuring. Please adapt, share, and enjoy~ This is also mirrored by [colleen lenin quine](https://quine.xyz/recipes/nemnems-pizza-dough.html), who provided their ingredients adjustments for a halved recipe!

<!--more-->
Yields about 2 larger pizzas, or 4-6 personal pan size.

Ingredients:

  -  3 + 3/4 cups of flour (all-purpose is okay, bread flour is fucking amazing)
   - 1 to 1+1/4 cups of warm water
 -   2+1/4 teaspoons (or 1 packet, usually) of yeast
  -  Olive oil (you shouldn't need more than a cup but -shrugs-)
   - 1-2 teaspoons of sugar (you can really eyeball this. also you can substitute with about the same amount of honey)
  -  1-2 teaspoons of salt (same. like more than a few shakes of a salt shaker? more or less isn't much harm unless you dump an entire salt shaker into it

Steps:

   - Prep 1 cup of warm water. I feel the temperature with the inside of my wrist, it should be warm but not scalding or near painful.
 -   Using the bowl of a food processor (I keep the blade in to start with), a stand mixer, or just a normal-ass mixing bowl, add 1 cup of water, 2 and a quarter teaspoons of yeast, and your 1-2 teaspoons of sugar. Mix it around a bit however is applicable, whether that's a quick pulse of the food processor/stand mixer, or just swirling the bowl around.
  -  Wait for the water to start foaming, it should take like 5 minutes. It won't be super bubbly, but it'll probably be recognizable.
  -  Prepare an extra 1/3 cup of water, then add the flour and salt (salt after because it slows down the yeast activating, whereas sugar acts as food) to the bowl, and a splash of olive oil to help avoid sticking to your hands and such. You can also cover your hands in olive oil to help here.
  -  Mix together however is applicable! Your dough will probably not all stick to itself or stay together well, and you'll end up with some dry flour. Slowly add water and knead it in. Remember, it's easier to add more water to a dough than to remove it!
 -   Remove mixer bits or food processor blades, and try to knead in any extra wet bits that got stuck in the machinery if applicable (happens a lot with the food processor in my experience). Splash on oil until it's a bit of a slippery coat around the whole thing preventing it from sticking to the bowl, then cover the bowl and let sit for 1 hour, until it's doubled in apparent size, or longer if you like. Letting it sit overnight gives really good results sometimes! It works in a flavor from the yeast fermenting.
 -   After you've waited, knead it into a ball, applying olive oil to your hands to avoid sticking as necessary. You can separate it out, either tearing by hand into roughly equal measures, or cutting with a bread knife. You can just work the individual pieces into balls, and freeze any you're not going to use right now.
 -   Let it sit for another 30 minutes or so!
 -  More kneading! What I do is get roughly a ball-like shape, and then keep folding the outer edges in and onto the bottom. If you have a flat work surface, it kind of works better that way. But basically just, get it so you have this round pebble-like shape of dough, where the bottom really is kind of a mess, continuously pushing more into the bottom so the top bit gets all stretched out. If you have it tear in a way where it struggles to stick together, a bit of water will help act as a glue. You can also flour your work surface for this! And you'll be roughly done when you can poke it and watch the dent bounce back up til it's all smooth and round again.
  -  Stretch it out onto your baking surface (more on that next step), get the rough shape you're going for, pick it up by the edge to stretch it out, et cetera. Just have fun~! You'll want it relatively thin, though of course the overall thickness preference varies. Your dough will rise a fair bit in the oven, so make sure to account for that. Super thick dough means VERY thick pizza, to where it might not cook all the way through.
  -  Now, there are a few options for surfaces to bake it on. I own a perforated pizza pan, which has a bunch of holes poked into it, to get the bottom crust as hot as the toppings. A baking stone is made to be put in the oven when you start pre-heating, and you can do the same thing by flipping a baking sheet pan upside-down. You can use a second baking sheet pan to slide it on top, like a pizza peel that they use in brick ovens. Also you can literally just put ot on a normal-ass pan without heating the pan, and it will be perfectly fine. Perforations and baking stones are just kind of that extra touch for a little bit more crispiness.
   - Preheat~! 500 degrees Fahrenheit/260 celsius or as hot as your oven goes up to that. If you're using a baking stone or cookie sheet subsitute, put that in the oven at the same time as you start preheating. Baking stones also take a lot longer to heat up, to where letting your oven heat for 30+ minutes might be good.
 -   Top that pizza!! (🐰 snrk.) No wrong answers. Put what makes you happy, whether that's tomato sauce and tomato slices or a fuckton of cheese, or whatever! Just try not to layer things on too thick, or it could impact the crust baking properly a little bit. There's a reason that when you order pizza from a place, the more toppings you add, the less of each topping you get.
  - Bake for 12-15 minutes! I set a timer for 10, then keep an eye on it from then on. If you have cheese on top, the cheese browning can server as a marker of being done, but the crust will start to crisp and get brown and thin parts of the sauce that may have ended up on the outer crust will kinda bake into it.
