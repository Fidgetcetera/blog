---
layout: post
title: "capitalism sucks for us cripples"
date: 2019-09-30
tags: [Politics, disability]
content-warnings: "capitalism and discussion of systemic ableism"
---

okay so capitalism fucking sucks and we hate it. that's like, pretty obvious about us. and we want to talk about another way to think about how it sucks, from the perspective of being a cripple.

<!--more-->we wanna also say that this is an idea we came up with mostly ourselves, but we also do think we aren't alone in coming to this conclusion and that more than likely other disabled anti-capitalists have come up with this, and it's really shit that we have not gotten to actually hear this from other people.

okay but basically the whole thesis here is: making exceptions for disabled people represents a flaw in how you structure your systems.

we'll go with college because of just dropping out recently - you know how some professors don't let you bring laptops or phones into a classroom? and pretty often they'll say "if you need it for accessibility you're an exception to the rule." this represents a flaw in the design of the class, placing disabled people at a disadvantage unless they convince their professor to let them be on equal footing with classmates. additionally, it singles out disabled people with a very visible marker of disability.

the default is inaccessibility and even anti-accessibility, and disabled people cause an interruption to that default. but additionally, this brings with it the fact that disabled people need to prove their disability. you can't just tell a professor you have a disability, you need a note from a doctor and paperwork from your school. even if, like us, you have visible physical proof of how your body is disabled, that isn't always going to be enough to be late for an exam you limp to when the professor locks the door by default.

there are sometimes simple, sometimes less simple solutions to these things, too. like, instead of banning laptops, you can section a part of the classroom off for laptops-only to prevent distraction of other students. instead of expanding test times for disabled people, you can do away with the current form of timed tests and grading systems. or like, design your test better as a stopgap i guess.

so, where does capitalism come into this? for you, capitalism is all about work and wage labor (unless you're rich and own means of production, in which case [pay us](https://patreon.com/fidgetcetera)). you must work to earn a wage from capitalists, unless they feel particularly [charitable](https://twitter.com/search?q=%23TransCrowdFund). and this presents a problem for disabled people, who may not have access or ability to work. if every job application says "must be able to lift 10 pounds of weight", guess who's not getting those jobs? if you're in a wheelchair and the buildings have stairs, well...

so many governments implement a system for disabled people to earn money outside of capitalism to survive. now, ignoring for a second that [at the least in america, this system does not actually do enough to keep disabled people out of poverty](https://www.alternet.org/2014/10/5-despicable-ways-we-keep-disabled-people-chained-poverty/), fundamentally you have the same issue as laptops in a classroom. Rather than society being accessible by default, you must prove your need for accessibility. you can't just not work, you must prove that you have a reason to not be working.

proving, here, involves both having a doctor define your disability to be bad enough not to work, *and* your government having a secondary layer of bureaucracy with government officials having to approve your doctor's own diagnosis. people, including doctors, are ableist, and they hold the power over disabled people being able to live at all.

society should not be structured around being required to do things that not everyone can do. forcing work upon people fucking sucks. instead, we should uplift disabled people, and everyone, by giving to each according to their need, from the work of each according to their ability.
