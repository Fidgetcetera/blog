---
layout: post
title: Tired of Politics
date: 2017-04-10
tags: [Politics]
content-warnings: "US Politics, neo-nazis and white supremacy, transmisia"
---
Tired, oh so tired. We've all been there. I think? I know I've been there, and I've been there for ages. Damn politics. So tiring, so frustrating, so dividing. Everyone's got their opinions and people just wanna get along like the old days, when people didn't get into massive fights over political issues. So why can't we all just hold hands and get along? Or just shut up and let people believe their bullshit?

<!--more-->Wellllll. I'd sure love to get along with people more. But when they keep doing shit like [deciding I shouldn't be able to use public restrooms](http://www.charlotteobserver.com/news/politics-government/article68401147.html), even at ridiculous costs to their own economy from people being suddenly a lot less willing to visit a place willing to restrict its citizens like this, it stops being so easy to say "Why don't we all just get along?" And while this is one specific case, a HEAVY amount of American politics have gone from mildly sketchy ground where people sometimes had some middle ground feelings about issues, to purely alienating with-or-against ideals.

So with pretty much every group getting fucked over by someone else in America getting fucked over extra hard, it's VERY hard for some of us to say "well howdy neighbor who voted for the scam-artist-reality-TV-star-turned-president who constantly tries to restrict rights for people, how's it going today?" It's hard not to avoid people who actively back the GOP while it attempts to end even the shoddiest bit of health care that this country has. It's sometimes hard not to fear them, especially when [white supremacists are rather popular among the right.](https://www.splcenter.org/fighting-hate/extremist-files/individual/richard-bertrand-spencer-0)

So yeah, I'm fucking tired of politics, and the people behind the current political issues today. But unfortunately, I can't just shut up and hide away and pretend politics don't exist, because the party in power in the US is a fucking nightmare of gerrymandering, corruption, and attempts to restrict the rights of citizens. I can't justify shutting up and hiding away and trying to live my life when the political climate is one of hatred and attempts to prevent me, and many others less privileged than myself, from existing at all. So I'm tired, but I'm not shutting the fuck up.
