---
layout: post
title: "Synonyms and Bigoted Language"
date: 2019-09-30
tags: [Politics, disability]
content-warnings: "Discussion of ableist slurs around intelligence."
author: nemmy
---
Hi! I'm Nemmy Nyms, witch of words. I'm a partition of Mx. Ace Fucking Jaycee, and I thought I should write a little thing about words.

<!--more-->
I think some people, when addressing their bigotry, really don't want to put in any effort to questioning themselves. This isn't going to be about defensiveness, or aimed at people who refuse to listen about ableism. I'm fucking tired of that, and my only message is that if you care about us, stop being so fucking ableist. But really I'm here for the people who keep bouncing from word to word, saying "oh, i didn't realize that was harmful" over and over.

When you're told a word does harm and you want to fix it, there's often this urge to just... Replace the word. In some cases this works! For example, I sometimes avoid calling partners of mine who are trans women "hon", because there's a specific harmful subculture that uses it as an insult. Swapping that in for "dear" or "love" or anything else that is a cute term of endearment for a partner that they like works out pretty well, because the issue isn't being affectionate with our partner, but that we used a word for that which has been poisoned for some by a sarcastic usage.

There are also sometimes where slurs and acceptance come into play. As a disabled person, I will tell people not to call me "handicapped". I dislike that term immensely, not because I want my disability ignored, but because "handicap" is a term for an artificial adjustment of play in sports, like putting guard rails up when bowling. Handicaps are a thing made to make games more fair, and that is a really uncomfortable way to think about disability.

But then there's insults. This is where issues really crop up a lot. A *lot.* I'm gonna assume you know my position on intelligence as a measure of worth and how it carries so much oppressive baggage, but if not, [this video on IQ](https://www.youtube.com/watch?v=lBo_RLzzk0E) is pretty good for the topic. I don't plan on debating this.

So, let's start on the r-slur. We used to use it in our early teens, and I still see it put on things as a suffix when not directly used outright. But for the most part, a lot of people at least left-of-center have really acknowledged that using it is bad. But... Why and how were they using it in the first place? "That's so [slur]", "Don't be such a [slur]", et cetera. The word being a slur against intellectually disabled people and used as a general insult is itself wrong, but what meaning does the r-slur actually take on here?

Luckily, everyone answers that for you by their replacement. People realized the r-slur was wrong to use, took that to mean that the *word* was the problem, and swapped it for intelligence insults like "idiot" that they were *already* using it interchangeably with. But, as [this Merriam-Webster article](https://www.merriam-webster.com/words-at-play/moron-idiot-imbecile-offensive-history) points out, several of those words have a really shitty clinical history around disabled people, too, because the entire concept of intelligence has a really shitty history around disabled people. And then even they end the article suggesting alternatives to these, such as "driveler" (listed as a synonym for [someone who] "lets saliva dribble from the mouth". Yikes?), and "chucklehead" (defined as "blockhead" which is then defined as... "a stupid person"!)

The problem with hunting for synonyms for insults is that you sometimes need to step back and ask: "What am I insulting this person for?" Intelligence is often a marker of some inherent, assumed-measurable amount of brain capacity. So you need to stop replacing the r-slur with "idiot" and then "peabrain" and then "dipshit" and start asking "why am I insulting this person based on some inherent measure of their being, rather than their actions of harm?" Maybe, when you've dealt with that, you'll actually sort your ableist shit out.
