---
layout: post
title: "Star Wars: The Last Jedi Thoughts"
date: 2017-12-18
tags: [Movies]
content-warnings: "Space fascist aesthetic discussion, death both in and out of fiction, Star Wars: The Last Jedi Spoilers"
---

So, I saw Star Wars: The Last Jedi! Just to get it out of the way, I did _not_ see _Star Wars: The Force Awakens_, I got a quick plot summary from my friend that covered the important bits. With that out of the way, here we go!

<!--more-->Right then. So I'm 19. I've never seen a Star Wars movie in theaters before last night. I saw IV, V, and VI on VHS tapes, and the prequels maybe on DVD? I don't really remember any of these movies well, and yet they were somewhat formative. Like, I remember pieces, and I remember media surrounding them, such as reading _the Glove of Darth Vader_ or playing Lego Star Wars on my DS and 100%ing it. I also haven't seen a star wars movie in years in general, seeing as I didn't watch _Awakens_.

So what was it like for me? I was angrily trying not to be nostalgic while tearing up at the opening crawl and that same music I knew well. By the end? I was **absolutely** humming along to the music and had a silly grin on my face. It was a magical experience. No, a force-filled experience. And I kinda feel conflicted about that.

See, I don't mind being nostalgic. I understand it's kinda a thing that happens. But the existence of a movie pulling your nostalgic heartstrings while being new feels... Manipulative. I don't want to feel nostalgic about _The Last Jedi_ because I don't want to be manipulated into doing free advertising about how I had a goofy grin on my face at all the cool _Star Wars_ things. So I don't know.

So, with that said, of course there's all the little things that made it _Star Wars_ and made me feel lots of feelings. So what was different?

Let's start with the space Nazis. The Empire. Now this is something where I couldn't tell how much was my own changes as a person and how much was intentional movie stuff, but the "cool" factor of the dark side of the force and the Empire felt way less existent. The space Nazis weren't portrayed as badasses put together well with a mighty emperor and a threat that might not necessarily be possible to overcome. The rebels were still on the run from them, but the Empire was being led by a kinda ugly dude with sideburns and bad plans, an emotionally unstable teenager, and an overconfident "supreme leader" who believes he can control people a lot better than he can. This Empire is not led by Vader and Palpatine, it's led by assholes with money and power who don't know what they're doing. Poe, our plucky pilot character, mocks them right at the start of the movie, and it's hilarious and satisfying and something that feels like it'd never happen with the old Empire. And I like that a lot. Fuck space Nazis.

The light side, the rebellion, the anti-fascist heroes of our story weren't all together, either. But they've always been a rag-tag team of fighters who learn and overcome obstacles, sometimes by having to run away. An escape is a victory in this movie, but while a victory does tend to happen at the end of a movie, it doesn't come without losses. The entirety of the rebellion's bombers get destroyed, the rebels get beaten back into a corner into an old abandoned base and then get forced to run away from that, with leaders dead and weapons for retaliation near non-existent. They're a little fucked and their last efforts to hold out against miniaturized Death Star technology smashing down the door to their bunker fail. The Jedi are, well, barely anything. But they've got hope, and they're still alive.

Speaking of the losses faced, there's something I need to talk about. A tension hangs over this movie which is one that ties in from the real world. Carrie Fisher, as you probably know, passed away at the end of last year. And that left a question hanging over my head through this whole movie. How will they handle that they don't have the actress for Princess Leia anymore? Will they rewrite things, let her die in the film? This is driven home especially with a near-death moment midway through the movie, where the bridge of the ship she's on is destroyed and she's out in space, until she reaches out with the force and flies back towards the door with the last grips of her strength. It's a bit tense in a way that, with no related real world context, would be a great moment, but instead it feels kind of awful. And well, apparently we'll just get to wait and find out what they're going to do about Princess Leia when they get around to Episode IX. It left a weird feeling in my gut, for sure.

I could probably talk about the plot elements and character development but I kinda ran out of word energy, and I won't feel accomplished if I leave this as a draft. So that's it for now! Maybe I'll do some big followup post about how gay I am for Rose another time.
