---
layout: post
title: Hey, I exist!
date: 2017-03-31
tags: [General, Updates]
---

Well, gosh! Hey, cuties. I guess I have a blogging space and site now. I really don't have a lot to say right now, but I'm sure I will soon. Feel free to hang around for my takes on society, music, video games, and whatever else I end up talkin' about.
