---
layout: post
title: Month's End, January 2018
date: 2018-02-10
tags: [General, Updates]
---
Ahhhh, a month into the year and here I am, late on a blog post again!! An update on my month lies below the cut.

<!--more-->

So, to start with, Chosa and I have been cutting back on the transposable episodes. It's now a bit less weekly, as you can tell by the last time an episode came out. We're looking at less scheduling it, and more recording an episode when an idea for a topic strikes, I think.
