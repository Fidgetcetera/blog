---
layout: post
title: Month's End, July 2017
date: 2017-07-29
tags: [General, Updates, Patreon Export]
content-warnings: "College"
---

Hey! I figured I'd give an update by the end of the month of what I've been up to, soooo:

<!--more-->This month I did a lot of planning for school stuff, which has left me with little time to make good internet content. However, I've still got things I've gotten done and I've had a bit more time now!

I've started knitting, and I'd like to prop up an etsy store to sell queer pride flag (and maybe video game character) inspired knit items, especially beanies but also laptop cozies, arm warmers, whatever I can! Of course, surely I'll make a reward for my lovely patrons that includes a discount code for the store.

I've started drawing! I'll post a WIP of a character I'm working on as a patron-only post, though I may only be posting some complete works on an 18+ account. More on that to follow depending on how much I draw of that kind of content!

I've got a podcast in the works! I can't talk about it much, but pretty much everywhere on social media you'll surely hear about it when it comes 'round.

This has been ongoing, but Saturdays when things don't go wrong, I'm playing Urban Shadows with my pals Taf_Kat, FigureFour, Tuxtradamus, and AxisCloud, which you can check out at 8 P.M. Eastern time on [Tux's Twitch page](https://twitch.tv/tuxtradamus)!

Annnd finally, while I haven't gotten much done with my website, I'd really like to redesign it. I just uh, don't really know how. But I'd like to separate the blog side out better, and really use it more. I have a lot of critical thoughts about things, and I'd like to get them out somewhere. Maybe a blog, maybe in video form, we'll see!

Finally, of course, thank you to everyone sticking around this month! <3
