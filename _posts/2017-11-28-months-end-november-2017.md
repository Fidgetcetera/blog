---
layout: post
title: Month's End, November 2017
date: 2017-11-28
tags: [General, Updates, Patreon Export]
content-warnings: "College"
---
Well, we're getting there anyway! Close enough.

<!--more-->IRL things first, finals are coming up really soon for me! I have 2 papers to finish by the 4th, actually. Oops. Anyway that means December I should have a lot more time to mess around and do things!!! My dad also gave me an antique cane that's been massively helping with my leg problems, so that's cool!!

Online, I've definitely had a bit less ability to be omnipresent and make or do things, but [Transposable](https://stipes.co/series/transposable) is mostly trucking along! In addition, I've started streaming occasionally to [Mixer](https://mixer.com/Fidgetcetera) which has been really fun! I try to stream to unlisted games so people I mostly know of show up. I don't really have a schedule, but I may set one up for my winter break, possibly. I also moved on Mastodon, hosting my own instance at [sins.center](https://sins.center/Fidgetcetera) for private use. And I sorta re-did my [website](https://acey.eerie.garden) ^w^

Oh yeah! I came out as aromantic and nonbinary. So those are fun!! I may [blog](https://acey.eerie.garden/blog) about that.
