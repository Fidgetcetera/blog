---
layout: post
title: "The Binding of Isaac is Weird."
date: 2018-07-14
tags: [Video Games, The Binding of Isaac]
content-warnings: "Discussion of and spoilers from the Binding of Isaac, which is a video game that requires warning for... Body horror, general grossness, feces, religion, abusive parents, abandonment, levels being body internals, birth, death, ableism, fatmisia, suicide, drugs, and like... Fuck, there's probably more. Mentions of Gamergate, a neo-nazi, and chronic pain + disability."
---

No, really, it's fucking weird. Like, the content warnings are indicative that this'll be a whole thing, but then there's the game's history, as this flash game that blew up and became a big ol' streaming thing and then got an expansion and then a remake in a new engine and then got two more expansions and there was an ARG somewhere in the mix?

<!--more-->The game's core is pretty simple. You're Isaac (until you unlock more characters), who's gone into a trapdoor in his bedroom to hide from his mother, who has stripped him of his belongings and clothing to serve God, and then asked for a sacrifice to prove her commitment. The game starts you naked, in a basement, and informs you of the controls as they're scrawled on the floor of the first room. You cry tears at enemies, which are fired out (but only horizontally and vertically, no diagonals), you can collect bombs, keys, and money, and find items. The items are randomly generated, the enemies are randomly generated, and the room layouts are randomly chosen.


It's just. There are a lot of items. Used to be 196 in the base Binding of Isaac, and as of the Afterbirth+ expansion, there are 547? That question mark isn't our unsureness, we trust [the wiki](https://bindingofisaacrebirth.gamepedia.com/Item)'s take on this. It's just. There are 547 items you can possibly pick up? That's kind of a lot. Almost all items have unique effects, with only a few recycling much at all. Some are even new takes on old items. For example, there's [Teleport](https://bindingofisaacrebirth.gamepedia.com/Item) which teleports you to a random room on the floor. And then [Teleport 2.0](https://bindingofisaacrebirth.gamepedia.com/Teleport_2.0) drops you into the next room that has not been explored already, with a set order of types of rooms that it will take you to. Each of these recharges after you clear rooms of enemies.

That's just two active ones. There's a lot of those. And then there's passive ones that stack! Like, [Brimstone](https://bindingofisaacrebirth.gamepedia.com/Brimstone) fires a thick laser beam (apparently it's made of blood). And [Spoon Bender](https://bindingofisaacrebirth.gamepedia.com/Spoon_Bender) will make your tears home in on enemies. So if you have both, your Brimstone laser will curve to chase enemies. And there's a shitton of combinations like that.


Oh, and then there's the "final" bosses, and the unlockability of everything. You beat one boss on your early runs, and then 2 more levels get added after that. And then more, and more, and branches to go to different places... We described this as "the final boss, the final final boss, the final final branch 1/3 final boss, the final final branch 2/3 final final boss, the final final branch 2/3 final final final boss, the final final branch 3/3 final final boss..." to some loved ones recently. You can unlock characters, which if you beat these bosses as specifically, you get specific items unlocked to collect on more runs, and the list of combinations of scenarios expands and expands.


An example of this - Just today, we played again. We beat the Hush. We did this as one of the unlockable characters, Judas, via mostly: An item that makes our tears into one giant tear that we can mind control and just float around, an item that makes our tears split when they hit enemies (so in this case, just extra tears constantly spawned if the ball was over someone), and one that made flies come out of us when we dealt damage. The Hush as a boss tends to sit really still, so we literally just sat the ball of tears on top of it. It was kind of ridiculous, and for our troubles, we unlocked an item called Betrayal, with no knowledge of what it does. It's just one more item in the pile, and it was specific to beating this boss as Judas.


It's a lot. We started playing it during the time when it streamed, and kinda obsessed over the flash game to some degree, with 144 hours of playtime on it. We had memorized nearly every single item in the original set of 196, plus other stuff that affects the game mechanics. We'd sit in streams, watch someone play, and go "Hey, do you mind if I tell you what's up with items if you don't know 'em?" and then excitedly share our knowledge.


The flash game used to have things where certain items would override others, actually, so like, that brimstone/spoon bender combo we mentioned would never work in the original - You'd just get a normal laser when you picked up brimstone. And we knew those so much! We recently booted up Rebirth, which we actually have 142 hours of playtime on. We've unlocked a bunch there, and honestly the game's like new for us, we pick up items and we know the mechanics of some, and have to guess by the game's weird logic at the hinting phrases that the game gives you, which really aren't helpful most of the time.


Another thing about this is that these "final" bosses each also have special endings, cutscenes that tell some conclusion to the mother attempting to sacrifice her child. There's a special way to beat Mom by using the bible item when in the fight with her, and the cutscene shows a bible falling off Isaac's shelf to knock her out, which is a bit representative of the original religious story the game gets its name from. Other cutscenes show Isaac finding himself dead, his mother finding him dead, him hanging himself, locking himself in a chest... Every character just looks like Isaac dressing up, too, which has led to a lot of fan theories about whether or not that's the case, with Isaac placing himself as different biblical figures. There's a lot to thematically take in about the game, to be honest, especially with Edmund McMillen (one of the two main designers who worked on the games) saying himself this [all had some personal experience of his childhood poured into it](https://www.eurogamer.net/articles/2012-06-29-the-binding-of-edmund-mcmillen).


Rebirth and the following expansions honestly feel like they muddy this for us. The game's theme exists but there's so much to take in about the game that trying to just parse the story and theme between 100+ failed runs is hard. The size of the game now and the amount of different ways you can end the game just kind of. Make it hard to parse. And we don't know if it was ever supposed to be easy? Video games are weird. This whole fuckin' game is weird.


The big final difference that's personal to us, between the original game vs Rebirth, is just that were fuckin' tired now, both of the games and in general. Isaac's one of those games that's very representative in our memory of a certain time period. It's a game that came out in 2011, and Rebirth came out in 2014, so we definitely played from like, ages 13 to 16, and probably a bit more. That's more than enough time to get burnt out on a video game, and those who know us to some degree may also know that this was a time when a very shitty person was in our life, and he's the one who introduced us to the game. It's also when we joined twitter, and ended up being part of the Gamergate hate mob.


Aside from all the memories this game holds of a fuckin' neo-nazi and gaming nerds on twitter, it's just... We don't have the energy to put hundreds and hundreds of hours into a video game anymore, and it's lonely to play one where the only multiplayer is offline. It's tiring to get our basic chores done around the house thanks to our leg pain, and it's draining to try to play a video game for multiple hours in a day, and it makes us kinda feel like shit for not getting something better done, sometimes, despite all that we try to do to remind ourselves that rest, relaxation, and play are important, especially with disability.


So it's... complicated. That one game we remember too well, and we'll play every once in a while for maybe an hour or two. It is and has been fun! Thanks to everyone whose work went into The Binding of Isaac, for making some teenage-years influential shit? We hope that's not too scary to think about. It probably is, all things considered.
