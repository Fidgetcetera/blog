---
layout: post
title: Quick Updates About Money
date: 2017-12-11
tags: [General, Updates]
content-warnings: "Money, patreon"
---
Hey y'all, this one's gonna be short! Basically, Patreon decided to fuck over small creators.

<!--more--> Given I was getting $12 a month, that kinda puts me in the "fucked over" portion. I'm gonna earn less under this new plan than I was originally, _and_ my patrons will be paying more. I ran through the math on it.

Soooo what do I do? Well, in the wake of this a lot of people have suggested things like keeping an eye on Kickstarter's currently-invite-only [Drip](https://d.rip/), or looking at open source alternatives such as [Liberapay](https://liberapay.com). Unfortunately, "here's a platform which is currently invite only" and "here's a platform which doesn't copy the reward model of patreon at all" are not amazing solutions. I'm lucky, I don't use Patreon rewards ever. But like, it's definitely worth noting that Liberapay isn't gonna be perfect for everyone.

That said, I have one set up. It's [here](https://liberapay.com/Fidgetcetera), and you can toss me money if you want. In light of this, I'll be trying to use the blog more rather than posting to my patreon, especially for my monthly update posts. I've exported my old monthly updates here, though you'll find some out of date info on them. If you'd like, though, you still _can_ give money to my [Patreon](https://patreon.com/Fidgetcetera). Hopefully things go well with all this!
